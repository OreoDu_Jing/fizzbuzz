import linecache

filename = input('Enter the file name:')
lines = open(filename).readlines()
dic = dict()
m = int(linecache.getline(filename,len(lines)))

for line in lines:
    line = line.strip('\n')
    if len(line) == 0 or line.find(':') == -1: continue
    pos = line.find(':')
    num = int(line[:pos])
    string = line[pos+1:]
    if m % num == 0:
        dic[num] = string

if len(dic) == 0: 
    print(m)
else:  
    print (''.join(dic[num] for num in sorted(dic.keys())))
